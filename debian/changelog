mrtdreader (0.1.6-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062961

 -- Benjamin Drung <bdrung@debian.org>  Wed, 28 Feb 2024 20:50:54 +0000

mrtdreader (0.1.6-3) unstable; urgency=medium

  * debian/control:
    - Correct Vcs URLs
    - New standards version 4.2.1 - no changes
    - Change email address of maintainer
  * debian/gbp.conf: Using master branch for debian now
  * debian/mrtdreader.1: More info in man page
  * debian/rules:
    - Remove get-orig-source target
    - Remove use of dpkg-parsechangelog
    - Remove trailing whitespace

 -- Ruben Undheim <rubund@debian.org>  Sat, 03 Nov 2018 13:54:55 +0100

mrtdreader (0.1.6-2) unstable; urgency=medium

  * debian/compat: 11
  * debian/control:
    - debhelper >= 11
    - New standards version 4.2.0 - no changes
    - VCS URL now pointing to salsa
  * debian/tests/control and debian/tests/can-link:
    - Added autopkgtest for testing that it is possible to
      link to the library libmrtd
  * debian/upstream/metadata: added metadata pointing to upstream

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 06 Aug 2018 22:12:12 +0200

mrtdreader (0.1.6-1) unstable; urgency=medium

  * New upstream release
    - Fixes some issues with UK and US passports at least
  * debian/control:
    - Changed Vcs-Git to https url
    - Removed -dbg package since -dbgsym is automatically built
  * debian/rules:
    - Remove override of dh_strip target

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 09 Feb 2016 20:12:49 +0100

mrtdreader (0.1.5-3) unstable; urgency=low

  * debian/control: Added "Multi-Arch: same" to allow libmrtd to be
    installed for multiple architectures

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 23 May 2015 22:14:25 +0200

mrtdreader (0.1.5-2) unstable; urgency=low

  * Uploaded to unstable
  * debian/mrtdreader.1: Fixed typo in man page

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 23 May 2015 11:08:15 +0200

mrtdreader (0.1.5-1) experimental; urgency=medium

  * New upstream release
    - patches/01_fix_regression.patch fixed upstream: removed
  * debian/control: Added "Suggests: graphicsmagick"
  * debian/copyright: Added year 2015

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 10 Jan 2015 13:12:21 +0100

mrtdreader (0.1.4-1) unstable; urgency=low

  * Initial release (Closes: #771708)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 13 Dec 2014 12:38:34 +0100
